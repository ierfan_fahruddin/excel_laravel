{{-- notifikasi sukses --}}
@if ($sukses = Session::get('sukses'))
<div class="alert alert-success alert-block">
    {{$sukses}}<i class="fa-solid fa-check"></i>
</div>
@endif