<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/profile', [ProfileController::class, 'index']);
Route::get('/profile/read', [ProfileController::class, 'read']);
Route::get('/profile/alert', [ProfileController::class, 'alerts']);
Route::get('/profile/export_excel', [ProfileController::class, 'export_exel']);
Route::post('/profile/import_excel', [ProfileController::class, 'import_excel']);
Route::get('/profile/delete/{id}', [ProfileController::class, 'destroy']);
