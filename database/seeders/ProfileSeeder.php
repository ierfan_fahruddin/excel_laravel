<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Psy\Util\Str;
use App\Models\Profile;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile')->insert([
            'name' => 'irfan',
            'usia' => 12,
            'alamat' => 'blingoh',
        ]);
        Profile::create([
            'name'      => 'irfan',
            'usia'       => '20',
            'alamat'     => 'blingoh',
        ]);
        Profile::create([
            'name'      => 'irfan2',
            'usia'       => '20',
            'alamat'     => 'blingoh',
        ]);
    }
}
