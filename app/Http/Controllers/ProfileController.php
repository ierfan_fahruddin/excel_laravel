<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ProfileExport;
use App\Imports\ProfileImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['profile'] = Profile::all();
        return view('profile', $data);
    }
    public function read()
    {
        $data['profile'] = Profile::all();
        return view('read', $data);
    }
    public function alerts()
    {
        return view('alert');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    function export_exel()
    {
        return Excel::download(new ProfileExport, 'profile.xlsx');
    }
    function import_excel(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        $file = $request->file('file');
        $nama_file = rand() . $file->getClientOriginalName();
        $file->move('file_profile', $nama_file);
        Excel::import(new ProfileImport, public_path('file_profile/' . $nama_file));
        Session::flash('sukses', 'data Berhasil Di impor');
        return redirect('/profile');
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $profile = Profile::find($id);
        $profile->delete();
        Session::keep('sukses', 'data Berhasil Di hapus');
        // return response()->with('sukses', 'data berhasil di hapus');
    }
}
